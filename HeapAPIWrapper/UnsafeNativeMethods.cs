﻿namespace HeapAPI {
    using System;
    using System.Runtime.InteropServices;
    internal static class UnsafeNativeMethods {
        [DllImport("KERNEL32.DLL")]
        internal static extern IntPtr HeapCreate(uint flOptions, int dwInitialSize, int dwMaximumSize);
        [DllImport("KERNEL32.DLL")]
        internal static extern IntPtr HeapAlloc(IntPtr hHeap, uint dwFlags, int dwBytes);
        [DllImport("KERNEL32.DLL")]
        internal static extern bool HeapFree(IntPtr hHeap, uint dwFlags, IntPtr lpMem);
        [DllImport("KERNEL32.DLL")]
        internal static extern IntPtr HeapReAlloc(IntPtr hHeap, uint dwFlags, IntPtr lpMem, int dwBytes);
        [DllImport("KERNEL32.DLL")]
        internal static extern bool HeapDestroy(IntPtr hHeap);
        [DllImport("KERNEL32.DLL")]
        internal static extern IntPtr GetProcessHeap();
        [DllImport("KERNEL32.DLL")]
        internal static extern int HeapCompact(IntPtr hHeap, uint dwFlags);
        [DllImport("KERNEL32.DLL")]
        internal static extern bool HeapLock(IntPtr hHeap);
        [DllImport("KERNEL32.DLL")]
        internal static extern bool HeapUnlock(IntPtr hHeap);
        [DllImport("KERNEL32.DLL")]
        internal static extern bool HeapValidate(IntPtr hHeap, uint dwFlags, IntPtr lpMem);
        [DllImport("KERNEL32.DLL")]
        internal static extern int HeapSize(IntPtr hHeap, uint dwFlags, IntPtr lpMem);
    }
}
