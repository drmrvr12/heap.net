﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeapAPI {
    /// <summary>
    /// A WIN32 Heap API Wrapper
    /// </summary>
    public class Heap : IDisposable {
        /// <summary>
        /// Initializes a new instance of the Heap class. Be sure to set the Handle before calling any methods in this class.
        /// </summary>
        protected Heap() { }
        /// <summary></summary>
        ~Heap() {
            Dispose();
        }
        /// <summary>
        /// Creates a private heap object that can be used by the calling process. The function reserves space in the virtual address space of the process and allocates physical storage for a specified initial portion of this block.
        /// https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapcreate
        /// </summary>
        /// <param name="flOptions">The heap allocation options. These options affect subsequent access to the new heap through calls to the heap functions. </param>
        /// <param name="dwInitialSize">The initial size of the heap, in bytes. This value determines the initial amount of memory that is committed for the heap. The value is rounded up to a multiple of the system page size. The value must be smaller than dwMaximumSize. If this parameter is 0, the function commits one page.To determine the size of a page on the host computer, use the GetSystemInfo function.</param>
        /// <param name="dwMaximumSize">The maximum size of the heap, in bytes. The HeapCreate function rounds dwMaximumSize up to a multiple of the system page size and then reserves a block of that size in the process's virtual address space for the heap. If allocation requests made by the HeapAlloc or HeapReAlloc functions exceed the size specified by dwInitialSize, the system commits additional pages of memory for the heap, up to the heap's maximum size. If dwMaximumSize is not zero, the heap size is fixed and cannot grow beyond the maximum size.Also, the largest memory block that can be allocated from the heap is slightly less than 512 KB for a 32 - bit process and slightly less than 1, 024 KB for a 64 - bit process.Requests to allocate larger blocks fail, even if the maximum size of the heap is large enough to contain the block. If dwMaximumSize is 0, the heap can grow in size.The heap's size is limited only by the available memory. Requests to allocate memory blocks larger than the limit for a fixed-size heap do not automatically fail; instead, the system calls the VirtualAlloc function to obtain the memory that is needed for large blocks. Applications that need to allocate large memory blocks should set dwMaximumSize to 0.</param>
        /// <returns>If the function succeeds, the return value is a handle to the newly created heap. If the function fails, the return value is NULL.To get extended error information, call GetLastError.</returns>
        public Heap(HeapCreationOptions flOptions, int dwInitialSize, int dwMaximumSize) {
            this.Handle = UnsafeNativeMethods.HeapCreate((uint)flOptions, dwInitialSize, dwMaximumSize);
            GC.KeepAlive(this);
        }

        /// <summary>
        /// Allocates a block of memory from a heap. The allocated memory is not movable.
        /// https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapalloc
        /// </summary>
        /// <param name="dwFlags">The heap allocation options. Specifying any of these values will override the corresponding value specified when the heap was created with HeapCreate.</param>
        /// <param name="dwBytes">The number of bytes to be allocated. If the heap specified by the hHeap parameter is a "non-growable" heap, dwBytes must be less than 0x7FFF8. You create a non-growable heap by calling the HeapCreate function with a nonzero value.</param>
        /// <returns>If the function succeeds, the return value is a pointer to the allocated memory block. If the function fails and you have not specified HEAP_GENERATE_EXCEPTIONS, the return value is NULL.</returns>
        public IntPtr Alloc(HeapCreationOptions dwFlags, int dwBytes) => UnsafeNativeMethods.HeapAlloc(Handle, (uint)dwFlags, dwBytes);

        /// <summary>
        /// Frees a memory block allocated from a heap by the HeapAlloc or HeapReAlloc function.
        /// https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapfree
        /// </summary>
        /// <param name="lpMem">A pointer to the memory block to be freed. This pointer is returned by the HeapAlloc or HeapReAlloc function. This pointer can be NULL.</param>
        /// <param name="HEAP_NO_SERIALIZE">Passes the HEAP_NO_SERIALIZE flag if true.</param>
        /// <returns>If the function succeeds, the return value is true. If the function fails, the return value is false. An application can call GetLastError for extended error information.</returns>
        public bool Free(IntPtr lpMem, bool HEAP_NO_SERIALIZE = false) => UnsafeNativeMethods.HeapFree(Handle, HEAP_NO_SERIALIZE ? (uint)1 : 0, lpMem);

        /// <summary>
        /// Reallocates a block of memory from a heap. This function enables you to resize a memory block and change other memory block properties. The allocated memory is not movable.
        /// https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heaprealloc
        /// </summary>
        /// <param name="dwFlags">The heap reallocation options. Specifying a value overrides the corresponding value specified in the flOptions parameter when the heap was created by using the HeapCreate function.</param>
        /// <param name="lpMem">A pointer to the block of memory that the function reallocates. This pointer is returned by an earlier call to the HeapAlloc or HeapReAlloc function.</param>
        /// <param name="dwBytes">The new size of the memory block, in bytes. A memory block's size can be increased or decreased by using this function. If the heap specified by the hHeap parameter is a "non-growable" heap, dwBytes must be less than 0x7FFF8. You create a non-growable heap by calling the HeapCreate function with a nonzero value.</param>
        /// <returns>If the function succeeds, the return value is a pointer to the reallocated memory block. If the function fails and you have not specified HEAP_GENERATE_EXCEPTIONS, the return value is NULL.</returns>
        public IntPtr ReAlloc(HeapCreationOptions dwFlags, IntPtr lpMem, int dwBytes) => UnsafeNativeMethods.HeapReAlloc(Handle, (uint)dwFlags, lpMem, dwBytes);

        /// <summary>
        /// Returns the size of the largest committed free block in the specified heap. If the Disable heap coalesce on free global flag is set, this function also coalesces adjacent free blocks of memory in the heap.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapcompact</para>
        /// </summary>
        /// <param name="HEAP_NO_SERIALIZE">Passes the HEAP_NO_SERIALIZE flag if true.</param>
        /// <returns>If the function succeeds, the return value is the size of the largest committed free block in the heap, in bytes.
        ///
        ///If the function fails, the return value is zero.To get extended error information, call GetLastError.
        ///
        ///In the unlikely case that there is absolutely no space available in the heap, the function return value is zero, and GetLastError returns the value NO_ERROR.</returns>
        public int Compact(bool HEAP_NO_SERIALIZE = false) => UnsafeNativeMethods.HeapCompact(Handle, HEAP_NO_SERIALIZE ? (uint)1 : 0);

        /// <summary>
        /// Heap handle.
        /// </summary>
        public IntPtr Handle { get; set; }
        /// <summary>
        /// Attempts to acquire the critical section object, or lock, that is associated with a specified heap.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heaplock</para>
        /// </summary>
        /// <returns>If the function succeeds, the return value is nonzero.
        ///
        /// If the function fails, the return value is zero.To get extended error information, call GetLastError.</returns>
        public bool Lock() => UnsafeNativeMethods.HeapLock(Handle);

        /// <summary>
        /// Releases ownership of the critical section object, or lock, that is associated with a specified heap. It reverses the action of the HeapLock function.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapunlock</para>
        /// </summary>
        /// <returns>If the function succeeds, the return value is nonzero.
        ///
        /// If the function fails, the return value is zero.To get extended error information, call GetLastError.</returns>
        public bool Unlock() => UnsafeNativeMethods.HeapUnlock(Handle);

        /// <summary>
        /// Validates the specified heap. The function scans all the memory blocks in the heap and verifies that the heap control structures maintained by the heap manager are in a consistent state. You can also use the HeapValidate function to validate a single memory block within a specified heap without checking the validity of the entire heap.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapvalidate</para>
        /// </summary>
        /// <param name="HEAP_NO_SERIALIZE">Passes the HEAP_NO_SERIALIZE flag if true.</param>
        /// <returns>If the specified heap or memory block is valid, the return value is nonzero.
        ///
        /// If the specified heap or memory block is invalid, the return value is zero.On a system set up for debugging, the HeapValidate function then displays debugging messages that describe the part of the heap or memory block that is invalid, and stops at a hard-coded breakpoint so that you can examine the system to determine the source of the invalidity.The HeapValidate function does not set the thread's last error value. There is no extended error information for this function; do not call GetLastError.</returns>
        public bool Validate(bool HEAP_NO_SERIALIZE = false) => Validate(IntPtr.Zero, HEAP_NO_SERIALIZE);

        /// <summary>
        /// Validates the specified heap. The function scans all the memory blocks in the heap and verifies that the heap control structures maintained by the heap manager are in a consistent state. You can also use the HeapValidate function to validate a single memory block within a specified heap without checking the validity of the entire heap.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapvalidate</para>
        /// </summary>
        /// <param name="lpMem">A pointer to a memory block within the specified heap. This parameter may be NULL.
        ///
        /// If this parameter is NULL, the function attempts to validate the entire heap specified by hHeap.
        /// 
        /// If this parameter is not NULL, the function attempts to validate the memory block pointed to by lpMem. It does not attempt to validate the rest of the heap.</param>
        /// <param name="HEAP_NO_SERIALIZE">Passes the HEAP_NO_SERIALIZE flag if true.</param>
        /// <returns>If the specified heap or memory block is valid, the return value is nonzero.
        ///
        /// If the specified heap or memory block is invalid, the return value is zero.On a system set up for debugging, the HeapValidate function then displays debugging messages that describe the part of the heap or memory block that is invalid, and stops at a hard-coded breakpoint so that you can examine the system to determine the source of the invalidity.The HeapValidate function does not set the thread's last error value. There is no extended error information for this function; do not call GetLastError.</returns>
        public bool Validate(IntPtr lpMem, bool HEAP_NO_SERIALIZE = false) => UnsafeNativeMethods.HeapValidate(Handle, HEAP_NO_SERIALIZE ? (uint)1 : 0, lpMem);

        /// <summary>
        /// Retrieves the size of a memory block allocated from a heap by the Heap.Alloc or Heap.ReAlloc function.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapsize</para>
        /// </summary>
        /// <param name="lpMem">A pointer to the memory block whose size the function will obtain. This is a pointer returned by the HeapAlloc or HeapReAlloc function. The memory block must be from the heap specified by the hHeap parameter.</param>
        /// <param name="HEAP_NO_SERIALIZE">Passes the HEAP_NO_SERIALIZE flag if true.</param>
        /// <returns></returns>
        public int Size(IntPtr lpMem, bool HEAP_NO_SERIALIZE = false) => UnsafeNativeMethods.HeapSize(Handle, HEAP_NO_SERIALIZE ? (uint)1 : 0, lpMem);

        /// <summary>
        /// Retrieves a handle to the default heap of the calling process. This handle can then be used in subsequent calls to the heap functions.
        /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-getprocessheap</para>
        /// </summary>
        /// <returns>If the function succeeds, the return value is a handle to the calling process's heap. 
        /// 
        /// If the function fails, the return value is NULL.To get extended error information, call GetLastError.</returns>
        public static Heap GetProcessHeap() => new Heap() { Handle = UnsafeNativeMethods.GetProcessHeap() };

        /// <summary>
        /// Free the unmanaged resources used by this object.
        /// </summary>
        public void Dispose() {
            UnsafeNativeMethods.HeapDestroy(Handle);
            GC.SuppressFinalize(this);
        }
    }
}
