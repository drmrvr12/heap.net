﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeapAPI {
    /// <summary>
    /// The heap allocation options. These options affect subsequent access to the new heap through calls to the heap functions. 
    /// <para>https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapcreate</para>
    /// </summary>
    public enum HeapCreationOptions : uint {
        /// <summary>
        /// No flags.
        /// </summary>
        NONE = 0x00000000,
        /// <summary>
        /// All memory blocks that are allocated from this heap allow code execution, if the hardware enforces data execution prevention. Use this flag heap in applications that run code from the heap. If HEAP_CREATE_ENABLE_EXECUTE is not specified and an application attempts to run code from a protected page, the application receives an exception with the status code STATUS_ACCESS_VIOLATION.
        /// </summary>
        HEAP_NO_SERIALIZE = 0x00000001,
        /// <summary>
        /// The system raises an exception to indicate failure (for example, an out-of-memory condition) for calls to HeapAlloc and HeapReAlloc instead of returning NULL.
        /// </summary>
        HEAP_GENERATE_EXCEPTIONS = 0x00000004,
        /// <summary>
        /// The allocated memory will be initialized to zero. Otherwise, the memory is not initialized to zero. This is used by Heap.Alloc and Heap.ReAlloc but not by Heap.Heap
        /// </summary>
        HEAP_ZERO_MEMORY = 0x00000008,
        /// <summary>
        /// Serialized access is not used when the heap functions access this heap. This option applies to all subsequent heap function calls. Alternatively, you can specify this option on individual heap function calls.
        ///The low-fragmentation heap (LFH) cannot be enabled for a heap created with this option.
        ///
        ///A heap created with this option cannot be locked. This is used by Heap.Heap but not by Heap.Alloc nor Heap.ReAlloc.
        /// </summary>
        HEAP_CREATE_ENABLE_EXECUTE = 0x00040000,



    }
}
