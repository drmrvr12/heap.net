﻿using System;
using System.Runtime.InteropServices;
using HeapAPI;

namespace HeapAPITest {
    class Program {
        static unsafe void Main(string[] args) {
            using (Heap heap = new Heap(HeapCreationOptions.HEAP_GENERATE_EXCEPTIONS, 0, 0)) {
                IntPtr memory = heap.Alloc(HeapCreationOptions.NONE, 512);
                byte[] text = System.Text.Encoding.ASCII.GetBytes("Hello, world!");

                //
                // Copy out text to a memory address allocated using Heap.Alloc
                //
                Marshal.Copy(text, 0, memory, text.Length);

                //
                // No cheating.
                //
                int len = text.Length;
                text = null;

                //
                // And copy the memory content to a new array.
                //
                byte[] outp = new Byte[len];

                for (int I = 0; I < len; I++) {
                    IntPtr ptr = memory + I;
                    byte* a = (byte*)ptr;
                    outp[I] = *a;
                }

                //
                // Decode
                //
                Console.WriteLine(System.Text.Encoding.ASCII.GetString(outp));

                //
                // And dispose
                //
                heap.Free(memory);

                Console.WriteLine();

                //
                // The following should either throw a memory violation or write random garbage
                //
                outp = new byte[len];
                for (int I = 0; I < len; I++) {
                    IntPtr ptr = memory + I;
                    byte* a = (byte*)ptr;
                    outp[I] = *a;
                }

                Console.WriteLine(System.Text.Encoding.ASCII.GetString(outp));

                while (true) {
                    //
                    // Out-of-memory machine
                    //
                    IntPtr mem = heap.Alloc(HeapCreationOptions.HEAP_GENERATE_EXCEPTIONS, 1024);
                    //
                    // This works correctly - if you uncomment the following, the program can just run forever and will not crash.
                    //
                    //heap.Free(mem);
                }
            }
        }
    }
}
